package beans;

import java.io.Serializable;

public class UserMessageCount implements Serializable {
    private int userId;
    private int messageCount;

	public int getMessageCount() {
		return messageCount;
	}
	public void setMessageCount(int messageCount) {
		this.messageCount = messageCount;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

}
