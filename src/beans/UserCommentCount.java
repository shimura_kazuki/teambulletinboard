package beans;

import java.io.Serializable;

public class UserCommentCount implements Serializable {
    private int userId;
    private int commentCount;

	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getCommentCount() {
		return commentCount;
	}
	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}

}