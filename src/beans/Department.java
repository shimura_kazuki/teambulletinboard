package beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Department  implements Serializable {

	private int id;
    private String departmentName;
    private Date createdDate;
    private Date updatedDate;

	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public List<Department> getDepartment() {
		return null;
	}

}
