package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Message;
import beans.UserMessageCount;
import exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message messages) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append(" title");
            sql.append(", text");
            sql.append(", category");
            sql.append(", user_id");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("  ?");                 // title
            sql.append(", ?");                 // text
            sql.append(", ?");                 // category
            sql.append(", ?");                 // user_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, messages.getTitle());
            ps.setString(2, messages.getText());
            ps.setString(3, messages.getCategory());
            ps.setInt(4, messages.getUserId());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	public void insert(Connection connection, int messageId) {
		PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM messages WHERE (id = ?);");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, messageId);
            ps.executeUpdate();

		} catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}

	public List<UserMessageCount> getUserMessageCount(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("user_id, ");
			sql.append("COUNT(id) ");
			sql.append("FROM messages ");
			sql.append("GROUP BY user_id;");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();

			List<UserMessageCount> ret = toUserMessageCountList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessageCount> toUserMessageCountList(ResultSet rs)
			throws SQLException {

		List<UserMessageCount> ret = new ArrayList<UserMessageCount>();
		try {
			while (rs.next()) {
				int id = rs.getInt("user_id");
				int count = rs.getInt("COUNT(id)");

				UserMessageCount messageCount = new UserMessageCount();
				messageCount.setUserId(id);
				messageCount.setMessageCount(count);

				ret.add(messageCount);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}