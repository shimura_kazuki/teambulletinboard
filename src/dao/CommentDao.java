package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import beans.UserCommentCount;
import exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment comment) {
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append(" text");
            sql.append(", user_id");
            sql.append(", message_id");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?");                  // text
            sql.append(", ?");                 // user_id
            sql.append(", ?");                 // message_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, comment.getText());
            ps.setInt(2, comment.getUserId());
            ps.setInt(3, comment.getMessageId());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void updateMessageTime(Connection connection, Comment comment) {
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE messages SET updated_date = CURRENT_TIMESTAMP WHERE id = ?;");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, comment.getMessageId());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	public void insert(Connection connection, int commentId) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM comments WHERE (id = ?);");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, commentId);
            ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<UserCommentCount> getUserCommentCount(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("user_id, ");
			sql.append("COUNT(id) ");
			sql.append("FROM comments ");
			sql.append("GROUP BY user_id;");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();

			List<UserCommentCount> ret = toUserCommentCountList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserCommentCount> toUserCommentCountList(ResultSet rs)
			throws SQLException {

		List<UserCommentCount> ret = new ArrayList<UserCommentCount>();
		try {
			while (rs.next()) {
				int id = rs.getInt("user_id");
				int count = rs.getInt("COUNT(id)");

				UserCommentCount commentCount = new UserCommentCount();
				commentCount.setUserId(id);
				commentCount.setCommentCount(count);

				ret.add(commentCount);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}