package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;


public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
        	 StringBuilder sql = new StringBuilder();
             sql.append("INSERT INTO users ( ");
             sql.append("account");
             sql.append(", name");
             sql.append(", password");
             sql.append(", branch_id");
             sql.append(", department_id");
             sql.append(", is_stopped");
             sql.append(", created_date");
             sql.append(", updated_date");
             sql.append(") VALUES (");
             sql.append("?"); // account
             sql.append(", ?"); // name
             sql.append(", ?"); // password
             sql.append(", ?"); // branch_id
             sql.append(", ?"); // department_id
             sql.append(", 0"); // is_stopped
             sql.append(", CURRENT_TIMESTAMP"); // created_date
             sql.append(", CURRENT_TIMESTAMP"); // updated_date
             sql.append(")");

             ps = connection.prepareStatement(sql.toString());

             ps.setString(1, user.getAccount());
             ps.setString(2, user.getName());
             ps.setString(3, user.getPassword());
             ps.setInt(4, user.getBranchId());
             ps.setInt(5, user.getDepartmentId());
             ps.executeUpdate();
         } catch (SQLException e) {
             throw new SQLRuntimeException(e);
         } finally {
             close(ps);
         }
     }


    public User getUser(Connection connection, String account, String password) {
        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE account = ? AND password = ?";
            ps = connection.prepareStatement(sql);
            ps.setString(1, account);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);

            if (userList.isEmpty()) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String account = rs.getString("account");
                String name = rs.getString("name");
                String password = rs.getString("password");
                int branchId = rs.getInt("branch_id");
                int departmentId = rs.getInt("department_id");
                int isStopped = rs.getInt("is_stopped");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User user = new User();

                user.setId(id);
                user.setAccount(account);
                user.setName(name);
                user.setPassword(password);
                user.setBranchId(branchId);
                user.setDepartmentId(departmentId);
                user.setIsStopped(isStopped);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

	public void update(Connection connection, User editUser) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  account = ?");
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", department_id = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			if ((!StringUtils.isEmpty(editUser.getPassword()))) {
				sql.append(", password = ?");
			}
			sql.append(" WHERE");
			sql.append(" id = "  + editUser.getId());

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, editUser.getAccount());
			ps.setString(2, editUser.getName());
			ps.setInt(3, editUser.getBranchId());
			ps.setInt(4, editUser.getDepartmentId());
			if ((!StringUtils.isEmpty(editUser.getPassword()))) {
				ps.setString(5, editUser.getPassword());
			}

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, int userId) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, userId);
			ResultSet rs = ps.executeQuery();
			User userInfo = toUser(rs);
			return userInfo;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	 private User  toUser(ResultSet rs)  throws SQLException {
		 User user = new User();
		 try {
			 while (rs.next()) {
					int id = rs.getInt("id");
					String account = rs.getString("account");
					String password = rs.getString("password");
					String name = rs.getString("name");
					int branchId = rs.getInt("branch_id");
					int departmentId = rs.getInt("department_id");
					user.setId(id);
					user.setAccount(account);
					user.setPassword(password);
					user.setName(name);
					user.setBranchId(branchId);
					user.setDepartmentId(departmentId);
				}
				return user;
		 } finally {
			 close(rs);
		 }
	 }

	public void changeState(Connection connection, User userState) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append(" is_stopped = ? ");
			sql.append(" WHERE ");
			sql.append(" id = ?;");
			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, userState.getIsStopped());
			ps.setInt(2, userState.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public boolean getExistUser(Connection connection, String account) {
	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE account = ?";
	        ps = connection.prepareStatement(sql);
	        ps.setString(1, account);
	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.size() >=  1) {
	        	return false;
	        }else {
	            return true;
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

}