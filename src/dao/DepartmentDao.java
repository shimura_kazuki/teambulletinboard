package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Department;
import exception.SQLRuntimeException;

public class DepartmentDao {
	 public List<Department> getDepartments(Connection connection, int num) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("SELECT * from departments ");
	            ps = connection.prepareStatement(sql.toString());

	            ResultSet rs = ps.executeQuery();
	            List<Department> ret = toDepartmentList(rs);

	            return ret;
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }

	    private List<Department> toDepartmentList(ResultSet rs) throws SQLException {

	        List<Department> ret = new ArrayList<Department>();
	        try {
	            while (rs.next()) {
	                int id = rs.getInt("id");
	                String departmentName = rs.getString("name");
	                Timestamp createdDate = rs.getTimestamp("created_date");
	                Timestamp updatedDate = rs.getTimestamp("updated_date");

	                Department department = new Department();

	                department.setId(id);
	                department.setDepartmentName(departmentName);
	                department.setCreatedDate(createdDate);
	                department.setUpdatedDate(updatedDate);

	                ret.add(department);
	            }
	            return ret;
	        } finally {
	            close(rs);
	        }
	    }
}
