package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {

	List<Branch> branches = new BranchService().getBranches();
 	List<Department> departments = new DepartmentService().getDepartments();

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	request.setAttribute("branches", branches);
    	request.setAttribute("departments", departments);
        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<String> messages = new ArrayList<String>();

        User user = new User();
        user.setName(request.getParameter("name"));
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setBranchId(Integer.valueOf(request.getParameter("branch_id")));
        user.setDepartmentId(Integer.valueOf(request.getParameter("department_id")));

        if ( isValid(request, messages) ) {
            new UserService().register(user);
            response.sendRedirect("manegement");
        } else {
            request.setAttribute("errorMessages", messages);
            request.setAttribute("user", user);
            request.setAttribute("branches", branches);
        	request.setAttribute("departments", departments);
            request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
    	String name = request.getParameter("name");
        String account = request.getParameter("account");
        String password = request.getParameter("password");
        String confirmPassword = request.getParameter("confirm_password");

        String branchId = request.getParameter("branch_id");
        String departmentId = request.getParameter("department_id");
        int confirmBranchId = Integer.valueOf(branchId);
        int confirmDepartmentId = Integer.valueOf(departmentId);

        if ( 10 <=  name.length() ) {
	    	messages.add("名前は10文字以下で入力してください");
	    } else if ( StringUtils.isEmpty(name) ) {
	    	messages.add("名前を入力してください");
	    }

        if ( !account.matches("^[a-zA-Z0-9]{6,20}$") )  {
	    	messages.add("アカウント名は半角英数字で6文字以上20文字以下で入力してください");
	    } else if ( StringUtils.isEmpty(account) ) {
	    	messages.add("アカウント名を入力してください");
	    }

        if ( !password.matches("^[a-zA-Z0-9]{6,20}$") )  {
	    	messages.add("パスワードは半角文字で6文字以上20文字以下で入力してください");
	    } else if ( StringUtils.isEmpty(password) ) {
	    	messages.add("パスワードを入力してください");
	    }

        if ( !password.equals(confirmPassword)  ) {
        	messages.add("パスワードと確認用パスワードが一致しません");
        } else if ( StringUtils.isEmpty(confirmPassword) ) {
        	messages.add("確認用パスワードを入力してください");
        }

        if (((confirmBranchId == 1) && (confirmDepartmentId ==  3)) || ((confirmBranchId == 1) && (confirmDepartmentId == 4)) ) {
        	messages.add("支店と部署の組み合わせが適切ではありません");
        }
        if (((confirmBranchId == 2 ) && (confirmDepartmentId == 1)) || ((confirmBranchId == 2) && (confirmDepartmentId == 2)) ) {
        	messages.add("支店と部署の組み合わせが適切ではありません");
        }
        if (((confirmBranchId == 3 ) && (confirmDepartmentId == 1)) || ((confirmBranchId == 3) && (confirmDepartmentId == 2)) ) {
        	messages.add("支店と部署の組み合わせが適切ではありません");
        }
        if (((confirmBranchId == 4 ) && (confirmDepartmentId == 1)) || ((confirmBranchId == 4) && (confirmDepartmentId == 2)) ) {
        	messages.add("支店と部署の組み合わせが適切ではありません");
        }

        boolean  existUser  =  new UserService().getExistUser(account);
        if ( existUser == false ) {
        	messages.add("すでに登録されたユーザーです");
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}