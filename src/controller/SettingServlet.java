package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;


@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String id = request.getParameter("userId");
		HttpSession session = request.getSession();
		String errorMessage = "不正なパラメーターが入力されました";

		if (StringUtils.isEmpty(id)) {
			session.setAttribute("errorMessages", errorMessage);
			response.sendRedirect("manegement");
			return;
		}
		if ( !id.matches("^[0-9]{1,4}$")) {
			session.setAttribute("errorMessages", errorMessage);
			response.sendRedirect("manegement");
			return;
		}
		if ( id.matches("^[a-zA-Z]+$")) {
			session.setAttribute("errorMessages", errorMessage);
			response.sendRedirect("manegement");
			return;
		}

        int userId = Integer.parseInt(request.getParameter("userId"));
        User user = new UserService().getUser(userId);

		List<Branch> branches = new BranchService().getBranches();
		List<Department> departments = new DepartmentService().getDepartments();

    	request.setAttribute("editUser", user);
    	request.setAttribute("branches", branches);
    	request.setAttribute("departments", departments);
    	request.getRequestDispatcher("setting.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		int userId = Integer.parseInt(request.getParameter("id"));
		User user = new UserService().getUser(userId);

        List<String> messages = new ArrayList<String>();
        List<Branch> branches = new BranchService().getBranches();
		List<Department> departments = new DepartmentService().getDepartments();

        User editUser = new User();
        editUser.setId(Integer.parseInt(request.getParameter("id")));
        editUser.setName(request.getParameter("name"));
        editUser.setAccount(request.getParameter("account"));
        editUser.setPassword(request.getParameter("password"));
        editUser.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
        editUser.setDepartmentId(Integer.parseInt(request.getParameter("department_id")));

		 if ( isValid(request, messages) ) {
			 new UserService().update(editUser);
			 response.sendRedirect("manegement");
	        } else {
	        	request.setAttribute("errorMessages", messages);
	        	request.setAttribute("editUser", editUser);
	            request.setAttribute("branches", branches);
	        	request.setAttribute("departments", departments);
	            request.getRequestDispatcher("setting.jsp").forward(request, response);
	        }
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String name = request.getParameter("name");
        String account = request.getParameter("account");
        String password = request.getParameter("password");
        String confirmPassword = request.getParameter("confirm_password");

        if ( 10 <=  name.length() ) {
	    	messages.add("名前は10文字以下で入力してください");
	    } else if ( StringUtils.isEmpty(name) ) {
	    	messages.add("名前を入力してください");
	    }

        if ( !account.matches("^[a-zA-Z0-9]{6,20}$") )  {
	    	messages.add("アカウント名は半角英数字で6文字以上20文字以下で入力してください");
	    } else if ( StringUtils.isEmpty(account) ) {
	    	messages.add("アカウント名を入力してください");
	    }

        if ( !StringUtils.isEmpty(password) ) {
        	if ( !password.matches("^[a-zA-Z0-9]{6,20}$") )  {
    	    	messages.add("パスワードは半角文字で6文字以上20文字以下で入力してください");
    	    }
        }

        if ( (!StringUtils.isEmpty(password)) ) {
        	if ( !password.equals(confirmPassword)  ) {
        		messages.add("パスワードと確認用パスワードが一致しません");
        	}
        }

        String branchId = request.getParameter("branch_id");
        String departmentId = request.getParameter("department_id");
        int confirmBranchId = Integer.valueOf(branchId);
        int confirmDepartmentId = Integer.valueOf(departmentId);

        if (((confirmBranchId == 1) && (confirmDepartmentId ==  3)) || ((confirmBranchId == 1) && (confirmDepartmentId == 4)) ) {
        	messages.add("支店と部署の組み合わせが適切ではありません");
        }
        if (((confirmBranchId == 2 ) && (confirmDepartmentId == 1)) || ((confirmBranchId == 2) && (confirmDepartmentId == 2)) ) {
        	messages.add("支店と部署の組み合わせが適切ではありません");
        }
        if (((confirmBranchId == 3 ) && (confirmDepartmentId == 1)) || ((confirmBranchId == 3) && (confirmDepartmentId == 2)) ) {
        	messages.add("支店と部署の組み合わせが適切ではありません");
        }
        if (((confirmBranchId == 4 ) && (confirmDepartmentId == 1)) || ((confirmBranchId == 4) && (confirmDepartmentId == 2)) ) {
        	messages.add("支店と部署の組み合わせが適切ではありません");
        }

        int userId = Integer.parseInt(request.getParameter("id"));
		User user = new UserService().getUser(userId);
		String a = user.getAccount();
		boolean  existUser  =  new UserService().getExistUser(account);
		if ( !account.equals(a)  ) {
			if ( existUser == false ) {
	        	messages.add("すでに登録されたユーザーです");
	        }
    	}

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}