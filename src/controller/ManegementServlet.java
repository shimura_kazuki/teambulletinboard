package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBranchDepartment;
import service.UserService;

@WebServlet(urlPatterns = { "/manegement" })
public class ManegementServlet extends HttpServlet {
	@Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

		int pageId = Integer.parseInt(request.getParameter("page"));
		int total = 5;
		if ( pageId != 1 ) {
			pageId = ( pageId - 1 ) * total + 1;
		}
//		 request.setAttribute("pageId", pageId);

		List<UserBranchDepartment> getUsers = new UserService().getUsers(pageId, total);
//		List<UserMessageCount>userMessageCounts = new MessageService().getUserMessageCount();
	//	List<UserCommentCount>userCommentCounts = new CommentService().getUserCommentCount();
        request.setAttribute("users", getUsers);
		//request.setAttribute("usermessagecounts", userMessageCounts);
		//request.setAttribute("usercommentcounts", userCommentCounts);
        HttpSession session = request.getSession();
        session.getAttribute("errorMessages");

        request.getRequestDispatcher("/manegement.jsp").forward(request, response);
    }
}
