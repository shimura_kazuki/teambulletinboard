package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/stop" })
public class StopServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

    	User userState = new User();
    	userState.setId(Integer.parseInt(request.getParameter("userId")));
    	userState.setIsStopped(Integer.parseInt(request.getParameter("isStopped")));
    	new UserService().changeState(userState);

        response.sendRedirect("manegement");
    }
}