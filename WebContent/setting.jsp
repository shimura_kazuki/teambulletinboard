<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<title>Insert title here</title>
</head>
<body>
	<header>
		<a href="manegement">ユーザー管理</a>
	</header>
    <div class="errorMessages">
		<c:if test="${ not empty errorMessages }">
			  <ul>
			      <c:forEach items="${errorMessages}" var="message">
			          <li><c:out value="${message}" />
			      </c:forEach>
			  </ul>
	    <c:remove var="errorMessages" scope="session" />
		</c:if>
    </div>
	<div class="form">
		<form action="setting" method="post">

 			<input type="hidden"  name="id" value="${editUser.id}"  /><br />

			<label for="account">アカウント</label>
			<input name="account" value="${editUser.account}"><br>

			<label for="password">パスワード</label>
			<input name="password"  type="password"><br>

			<label for="confirm_password">確認用パスワード</label>
			<input name="confirm_password"  type="password"><br>

			<label for="name">名前</label>
			<input name="name" value="${editUser.name}" ><br>

			<label for="branch_id">支社</label>
			<select name="branch_id">
				<c:forEach items="${branches}" var="branch">
					<c:if test="${ branch.id == editUser.branchId }">
						<option  value="${branch.id}" selected  id="branch_id">${branch.branchName }</option>
					</c:if>
					<c:if test="${ editUser.id != loginUser.id }">
						<c:if test="${ branch.id != editUser.branchId }">
							<option  value="${branch.id}"  id="branch_id">${branch.branchName }</option>
						</c:if>
					</c:if>
			    </c:forEach>
			</select>

			<label for="department_id">部署</label>
			<select name="department_id">
				<c:forEach items="${departments}" var="department">
					<c:if test="${ department.id == editUser.departmentId }">
						<option  value="${department.id}" selected  id="departmentId">${department.departmentName }</option>
					</c:if>
					<c:if test="${ editUser.id != loginUser.id }">
						<c:if test="${ department.id != editUser.departmentId }">
							<option  value="${department.id}"  id="departmentId">${department.departmentName }</option>
						</c:if>
					</c:if>
			    </c:forEach>
			</select>

			<input type="submit" value="更新">
		</form>
	</div>
 	<div class="copyright"> Copyright(c) Kazuki Shimura </div>
</body>
</html>

